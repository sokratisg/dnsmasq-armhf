# dnsmasq-armhf

Alpine arm32v6 Docker image for [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html).

## Usage

dnsmasq requires `NET_ADMIN` capabilities.

`docker run --restart unless-stopped -p 53:53/tcp -p 53:53/udp --cap-add=NET_ADMIN --name dnsmasq -d sokratisg/dnsmasq-armhf:latest`

To see command-line arguments, run `docker run --rm sokratisg/dnsmasq-armhf --help`.
Additional configuration can be specified in `/etc/dnsmasq.conf`.
