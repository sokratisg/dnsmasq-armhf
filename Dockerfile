FROM arm32v6/alpine:3.8
MAINTAINER Sokratis Galiatsis <sokratisg@hotmail.com>
RUN apk add --no-cache dnsmasq
COPY dnsmasq.conf /etc/dnsmasq.conf
COPY resolv.dnsmasq /etc/resolv.dnsmasq
EXPOSE 53 53/udp
ENTRYPOINT ["dnsmasq", "-k"]
